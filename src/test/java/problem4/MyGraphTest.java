package problem4;

import junit.framework.TestCase;

/**
 * Created by Olga Lisovskaya on 12/3/17.
 */
public class MyGraphTest extends TestCase {
    public void testGetConnectedComponents() {
        MyGraph<Integer> graph = new MyGraph<>(new Integer[]{0,1,2,3,4,5},
                new int[][]{{0,1},{0,2},{0,3},{3,1},{3,2},{4,5}});
        assertEquals("Wrong connected components.", "[[0, 1, 2, 3], [4, 5]]",
                graph.getConnectedComponents().toString());
    }
}
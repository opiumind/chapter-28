package problem4;

/**
 * (Find connected components) Create a new class named MyGraph as a subclass
 of UnweightedGraph that contains a method for finding all connected com-
 ponents in a graph with the following header:
 public List<List<Integer>> getConnectedComponents();
 The method returns a List<List<Integer>> . Each element in the list is
 another list that contains all the vertices in a connected component. For exam-
 ple, for the graph in Figure 28.21b, getConnectedComponents() returns
 [[0, 1, 2, 3], [4, 5]] .
 *
 */
public class App 
{
    public static void main( String[] args ) {
        MyGraph<Integer> graph = new MyGraph<>(new Integer[]{0,1,2,3,4,5},
                new int[][]{{0,1},{0,2},{0,3},{3,1},{3,2},{4,5}});
        System.out.println("Connected vertices: " + graph.getConnectedComponents());
    }
}
